from django.test import RequestFactory, TransactionTestCase

from datacenter.models.main import Device, Rack

"""
    # You need 1 file in order to run all the tests:
    #     # a python dict
    #     - tests/assets_tests/test_data_for_models_creation.py
"""

# tests class in the other files will
# inherit from this class


class DatacenterTest(TransactionTestCase):

    reset_sequences = True

    def make_devices(self, data_devices, rack):

        for i in range(len(data_devices.keys())):
            if rack is not None:
                rack = rack
            else:
                rack = Rack.objects.get(pk=data_devices[i]["rack"])

            self.device = Device.objects.create(
                device_type=data_devices[i]["device_type"],
                model_name=data_devices[i]["model_name"],
                number_of_ports=data_devices[i]["number_of_ports"],
                vertical_size=data_devices[i]["vertical_size"],
                in_state=data_devices[i]["in_state"],
                rack=rack,
            )

    def make_racks(self, data_racks):

        for i in range(len(data_racks.keys())):
            rack = Rack.objects.create(
                physical_location=data_racks[i]["physical_location"],
                available_vertical_units=data_racks[i]["available_vertical_units"],
                in_state=data_racks[i]["in_state"],
            )

    # create entries in db to test models creation
    # use them in all further tests
    def setUp(self):

        # instanciate request factory
        self.factory = RequestFactory()

        return super().setUp()
