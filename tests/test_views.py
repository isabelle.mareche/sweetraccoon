from bs4 import BeautifulSoup
from datacenter.models.main import Device, Rack
from datacenter.views import (
    DeviceCreateView,
    DeviceUpdateView,
    RackCreateView,
    RackUpdateView,
)
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.messages.storage.fallback import FallbackStorage
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpResponse
from django.test import Client

from .assets_tests.test_data_for_models_creation import data_devices, data_racks
from .test_setup import DatacenterTest


class DatacenterViewsTest(DatacenterTest):

    def test_number_initial_racks_and_devices(self):
        """test number of devices in db"""
        print(f"\t\tIN test_number_initial_racks_and_devices")

        # create racks from assets' rack dict
        self.make_racks(data_racks)
        # get all racks
        self.racks = Rack.objects.all()
        # create devices for each rack from assets' device dict
        for rack in self.racks:
            self.device = self.make_devices(data_devices, rack)
        # get all devices
        self.devices = Device.objects.all()

        # check if creation of racks and devices had successed
        self.assertEqual(self.racks.count(), 5)
        self.assertEqual(self.devices.count(), 40)

        # delete all instances
        self.devices.delete()
        self.racks.delete()
        # check if deletion had successed
        self.assertEqual(self.racks.count(), 0)
        self.assertEqual(self.devices.count(), 0)

    def test_create_new_rack_success(self):
        print(f"\t\tIN test_create_new_rack_success")

        request = self.factory.post(f"/rack-create/")

        request.POST = {
            "rack-physical_location": data_racks[0]["physical_location"],
            "rack-available_vertical_units": data_racks[0]["available_vertical_units"],
            "rack-in_state": data_racks[0]["in_state"],
        }

        """Annotate a request object with a session"""
        middleware = SessionMiddleware(request)
        middleware.process_request(request)
        request.session.save()

        """Annotate a request object with a messages"""
        middleware = MessageMiddleware(request)
        request.session.save()
        request._messages = FallbackStorage(request)

        response = RackCreateView.as_view()(request)

        self.rack = Rack.objects.last()

        self.assertEqual(
            data_racks[0]["physical_location"], self.rack.physical_location
        )
        self.assertEqual(
            data_racks[0]["available_vertical_units"],
            self.rack.available_vertical_units,
        )
        self.assertEqual(
            data_racks[0]["in_state"],
            self.rack.in_state,
        )

    def test_create_new_rack_fails(self):
        """case empty post"""
        print(f"IN test_create_new_rack_fails")

        data = {
            "physical_location": "",
            "available_vertical_units": "",
            "in_state": "",
        }

        # using simple request client
        request = Client()
        response_step1 = request.post("/datacenter/rack-create/", data)

        self.assertIsInstance(response_step1, HttpResponse)
        self.assertEqual(response_step1.status_code, 200)

        soup = BeautifulSoup(response_step1.content, "html.parser")
        wanted_error = "This field is required."

        self.assertIn(wanted_error, str(soup.body))

    def test_update_rack_success(self):
        print(f"\t\tIN test_update_rack_success")

        # create racks from assets' rack dict
        self.make_racks(data_racks)
        # get the rack to which the device will belong
        self.rack = Rack.objects.get(pk=3)
        print(
            f"\t\tIN test_update_rack_success\t\tself.rack::: {self.rack} pk={self.rack.pk}"
        )

        request = self.factory.post(f"/rack-update/")
        request.POST = {
            "rack-physical_location": f'{data_racks[1]["physical_location"]}-bis',
            "rack-available_vertical_units": data_racks[1]["available_vertical_units"],
            "rack-in_state": data_racks[1]["in_state"],
        }

        """Annotate a request object with a session"""
        middleware = SessionMiddleware(request)
        middleware.process_request(request)
        request.session.save()

        """Annotate a request object with a messages"""
        middleware = MessageMiddleware(request)
        request.session.save()
        request._messages = FallbackStorage(request)

        # add rack's pk in kwargs
        response = RackUpdateView.as_view()(request, **{"pk": self.rack.pk})

        # get the rack which has been updated
        self.rack_updated = Rack.objects.get(pk=3)

        self.assertEqual(
            f'{data_racks[1]["physical_location"]}-bis',
            self.rack_updated.physical_location,
        )
        self.assertEqual(
            data_racks[1]["available_vertical_units"],
            self.rack_updated.available_vertical_units,
        )
        self.assertEqual(
            data_racks[1]["in_state"],
            self.rack_updated.in_state,
        )

    def test_create_new_device_success(self):
        print(f"IN test_create_new_device_success")

        # create racks from assets' rack dict
        self.make_racks(data_racks)
        # get the rack to which the device will belong
        self.rack = Rack.objects.get(pk=1)

        request = self.factory.post("/datacenter/device-create/")
        request.POST = {
            "device-device_type": data_devices[0]["device_type"],
            "device-model_name": data_devices[0]["model_name"],
            "device-number_of_ports": data_devices[0]["number_of_ports"],
            "device-vertical_size": data_devices[0]["vertical_size"],
            "device-in_state": data_devices[0]["in_state"],
            "device-rack": 1,
            "submit_device": "",
        }

        """Annotate a request object with a session"""
        middleware = SessionMiddleware(request)
        middleware.process_request(request)
        request.session.save()

        """Annotate a request object with a messages"""
        middleware = MessageMiddleware(request)
        request.session.save()
        request._messages = FallbackStorage(request)

        response = DeviceCreateView.as_view()(request)

        self.device = Device.objects.last()

        self.assertEqual(data_devices[0]["device_type"], self.device.device_type)
        self.assertEqual(
            data_devices[0]["model_name"],
            self.device.model_name,
        )
        self.assertEqual(
            data_devices[0]["number_of_ports"],
            self.device.number_of_ports,
        )
        self.assertEqual(
            data_devices[0]["vertical_size"],
            self.device.vertical_size,
        )
        self.assertEqual(
            data_devices[0]["in_state"],
            self.device.in_state,
        )
        self.assertEqual(
            data_devices[0]["rack"],
            self.device.rack.pk,
        )

    def test_create_new_device_overflow_fails(self):
        print(f"IN test_create_new_device_overflow_fails")

        # create racks from assets' rack dict
        self.make_racks(data_racks)
        # get the rack to which the device will belong
        self.rack = Rack.objects.get(pk=1)
        # set the available size lower than the device size (6)
        self.rack.available_vertical_units = 4
        self.rack.save()

        request = self.factory.post("/datacenter/device-create/")
        request.POST = {
            "device-device_type": data_devices[3]["device_type"],
            "device-model_name": data_devices[3]["model_name"],
            "device-number_of_ports": data_devices[3]["number_of_ports"],
            "device-vertical_size": data_devices[3]["vertical_size"],
            "device-in_state": data_devices[3]["in_state"],
            "device-rack": 1,
            "submit_device": "",
        }

        """Annotate a request object with a session"""
        middleware = SessionMiddleware(request)
        middleware.process_request(request)
        request.session.save()

        """Annotate a request object with a messages"""
        middleware = MessageMiddleware(request)
        request.session.save()
        request._messages = FallbackStorage(request)

        response = DeviceCreateView.as_view()(request)

        self.msgs = []
        for msg in request._messages:
            self.msgs.append(str(msg))

        self.device = Device.objects.last()
        self.assertIsNot(self.device, Device)
        self.assertIs(self.device, None)

        wanted_msg = "['Used units: 6 but available vertical size in Rack «PARIS-DC-1-ALLEE-12-RG-4 [4U]»: 4U, OVERFLOW: 2. NOT SAVED']"

        self.assertEqual(wanted_msg, self.msgs[0])

    def test_update_device_success(self):
        print(f"\t\tIN test_update_device_success")

        # create racks from assets' rack dict
        self.make_racks(data_racks)
        # get the rack to which the device will belong
        self.rack = Rack.objects.get(pk=3)

        self.make_devices(data_devices={0: data_devices[3]}, rack=None)

        self.device = Device.objects.get(pk=1)
        print(
            f"\t\tIN test_update_device_success\t\tself.rack::: {self.rack} pk={self.rack.pk} self.device::: {self.device} pk={self.device.pk}"
        )

        request = self.factory.post(f"/device-update/")
        request.POST = {
            "device-device_type": data_devices[3]["device_type"],
            "device-model_name": data_devices[3]["model_name"],
            "device-number_of_ports": data_devices[3]["number_of_ports"],
            "device-vertical_size": data_devices[3]["vertical_size"],
            "device-in_state": data_devices[3]["in_state"],
            "device-rack": 3,
            "submit_device": "",
        }

        """Annotate a request object with a session"""
        middleware = SessionMiddleware(request)
        middleware.process_request(request)
        request.session.save()

        """Annotate a request object with a messages"""
        middleware = MessageMiddleware(request)
        request.session.save()
        request._messages = FallbackStorage(request)

        # add device's and rack's ids in kwargs
        response = DeviceUpdateView.as_view()(
            request, **{"pk_device": self.device.pk, "pk_rack": self.rack.pk}
        )

        # get the rack which has been updated
        self.device_updated = Device.objects.get(pk=1)

        self.assertEqual(
            data_devices[3]["device_type"], self.device_updated.device_type
        )
        self.assertEqual(
            data_devices[3]["model_name"],
            self.device_updated.model_name,
        )
        self.assertEqual(
            data_devices[3]["number_of_ports"],
            self.device_updated.number_of_ports,
        )
        self.assertEqual(
            data_devices[3]["vertical_size"],
            self.device_updated.vertical_size,
        )
        self.assertEqual(
            data_devices[3]["in_state"],
            self.device_updated.in_state,
        )
        self.assertEqual(
            data_devices[3]["rack"],
            self.device_updated.rack.pk,
        )
