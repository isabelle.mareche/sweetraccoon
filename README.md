
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
# sweetraccoon

# Demo project for Datacenter's racks and devices gestion

- Rack (physical rack): available vertical units (in U), devices
- Device (physical device): type (switch or router), model name, number of ports, vertical size (in U)

- The homepage displays all the racks (id, total vertical units, used vertical units, number
of devices)
- Selecting one will open a detailed view, showing a list of all the devices along
with their respective properties.
- Create / update and delete all these objects could be possible (not really delete, for some of them, in fact)
- These two models' lists are exposed through a read-only API (Django Rest Framework)

- Authentication is not implemented and it this demo project doesn’t try to look pretty.


![](static/images/demo/sweetracoon_racks.png)

## Getting started: clone this repository, make virtualenv and install requirements

- Make a Python virtualenv with Python 3.11 or 3.12 and install requirements
```shell
# first clone the repo
git clone https://gitlab.com/isabelle.mareche/sweetraccoon.git
# get inside the local directory that has been created
cd sweetraccon
# make a virtualenv, with virtualenvwrapper it is like this
mkvirtualenv sweetraccon
# this previous command should have activated the virtualenv, but if not
workon sweetraccon
# then install the required dependencies for the project
pip install -r requirements.txt
```

- Create .env file
```shell
# copy model for .env file and edit it if needed
# it will get used in settings for starting the project
cp .env.model .env
```

- Settle the Django project's prerequires
```shell
# migrations's file are not pushed to this remote repository
# so run the command to add migrations' file to your local repository
python manage.py makemigrations
# then apply the schema to db.sqlite3 which is not in the remote repo neither
# but will be re-created by Django in your local repository if missing
python manage.py migrate
# now you could add default's entries in database in order to test one or two things
python manage.py loaddata fixtures/*.json
# if you want to add your own user in Django's administration interface
python manage.py createsuperuser

# collect the static files provided by Django itself
python manage.py collectstatic

# finally run the Django server
# choose the same port as declared in .env for CORS_ORIGIN_WHITELIST
python manage.py runserver 8000
```

- Take a look at core/settings/main.py for pagination in list, lifecycle's states to count or exclude and definition of color's tags for states
```python

### start config initial ###
ALLOW_API_READ_WITHOUT_AUTHENTICATION = True
PAGINATE_BY_CBV_RACKS = 25
PAGINATE_BY_CBV_DEVICES = 25
IN_STATE_TO_COUNT_IN_USED_UNITS = ["PLA", "PRO", "PRE", "MAI"]
IN_STATE_TO_EXCLUDE_IN_USED_UNITS = ["REM", "DEC"]
IN_STATE_DEVICES_TO_EXCLUDE_INSIDE_RACKS = ["REM", "DEC"]
IN_STATE_COLORS_TAGS = {
    "PLA": "bg-info",
    "PRO": "bg-info",
    "PRE": "bg-success",
    "MAI": "bg-warning",
    "REM": "bg-secondary",
    "DEC": "bg-secondary",
}
### end start config initial ###
```
