from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from simple_history.admin import SimpleHistoryAdmin

from .models.main import Device, Rack
from .models.user import User


@admin.register(User)
class UserAdmin(UserAdmin):
    readonly_fields = [
        "pk",
    ]
    list_display = ["pk", "username", "last_name", "email"]
    search_fields = ["pk", "username", "last_name", "email"]


class DeviceInline(admin.TabularInline):
    model = Device

    def get_extra(self, request, obj=None, **kwargs):
        """Override the builtin method in order to set extra field number to 1 if creating and 0 if updating."""
        if obj:
            return 0
        else:
            return 1


@admin.register(Rack)
class RackAdmin(SimpleHistoryAdmin):
    inlines = [DeviceInline]
    list_display = [
        "pk",
        "physical_location",
        "available_vertical_units",
        "in_state",
    ]
    search_fields = ["pk", "physical_location", "in_state"]
