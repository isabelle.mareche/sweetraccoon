import logging

from core.settings.main import (
    ALLOW_API_READ_WITHOUT_AUTHENTICATION,
    PAGINATE_BY_CBV_DEVICES,
    PAGINATE_BY_CBV_RACKS,
)
from django.contrib import messages
from django.shortcuts import redirect, reverse
from django.views.generic.edit import CreateView, DeleteView, FormView, UpdateView
from django.views.generic.list import ListView
from rest_framework import permissions, viewsets

from .forms import DeviceCreateForm, DeviceUpdateForm, RackForm
from .mixins import MixinDevicePost, MixinPageTitle, MixinRackPost, MixinSuccessMsg
from .models.main import Device, Rack
from .serializers import DeviceSerializer, RackSerializer

logger = logging.getLogger(__name__)


class RackListView(MixinPageTitle, ListView):
    """View to render home page with paginated list of racks
        order by descendant ID.

    Args:
        paginate_by:
            Defined in conf_initial dict in core.config file by the key PAGINATE_BY_CBV_RACKS.
        page_title:
            type: str
            Model's verbose name, pluralize if needed, auto set by MixinPageTitle (self.get_queryset_page_title()).

    Returns:
        List of active racks with annotation for number of devices
        and total of used vertical units per rack.
    """

    model = Rack
    template_name = "datacenter/racks-list.html"
    context_object_name = "racks"
    paginate_by = PAGINATE_BY_CBV_RACKS
    action_name = "list"

    def get_context_data(self, **kwargs):
        """Set context page title and queryset for HTML page."""
        context = super().get_context_data(**kwargs)
        self.queryset = Rack.objects.get_annotate_racks()
        context["racks"] = self.queryset
        return context


class DevicesList(MixinPageTitle, ListView):
    """View to render paginated list of devices
       order by their rack's physical location and ID.

    Args:
        paginate_by:
            Defined in conf_initial dict in core.config file by the key PAGINATE_BY_CBV_DEVICES.
        page_title:
            type: str
            Model's verbose name, pluralize if needed, auto set by MixinPageTitle (self.get_queryset_page_title()).

    Returns:
        List of active devices with theirs associated racks.
    """

    model = Device
    template_name = "datacenter/devices-list.html"
    context_object_name = "devices"
    paginate_by = PAGINATE_BY_CBV_DEVICES
    action_name = "list"

    def get_context_data(self, **kwargs):
        """Set context page title and queryset for HTML page."""
        context = super().get_context_data(**kwargs)
        self.queryset = Device.objects.get_active_devices()
        context["devices"] = self.queryset
        return context


class RackCreateView(
    MixinPageTitle,
    MixinSuccessMsg,
    CreateView,
):
    """View to create new Rack.

    Args:
        msg:
            type: str
            For display or log errors
        action_name:
            type: str
            Name to display in HMTL messages and title
        form_action:
            type: str
            URL name for use in HTML form's action attribute in HTML detail page

    Returns:
        A new Rack's instance along with its associated first device
        and redirects to datacenter-rack-detail
    """

    model = Rack
    template_name = "datacenter/rack-detail.html"
    form_class = RackForm
    action_name = "create"
    msg = ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_action"] = "datacenter:rack-create"
        return context


class RackUpdateView(
    MixinPageTitle,
    MixinRackPost,
    UpdateView,
):
    """View to update a Rack.

    Args:
        msg:
            type: str
            For display or log errors
        action_name:
            type: str
            Name to display in HMTL messages and title
        form:
            type: ModelForm
            A RackForm to create the new Rack.
        form_device:
            type: ModelForm
            A DeviceCreateForm to create another Device at a time.
        form_action:
            type: str
            URL name for use in HTML form's action attribute in HTML detail page

    Returns:
        The Rack's instance along with its associated device(s)
        and redirects to datacenter-rack-detail
    """

    model = Rack
    template_name = "datacenter/rack-detail.html"
    form_class = RackForm
    action_name = "update"
    msg = ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_action"] = "datacenter:rack-update"
        return context

    def dispatch(self, request, *args, **kwargs):
        self.pk = self.kwargs.get("pk", None)
        self.object = Rack.objects.get(pk=self.pk)

        return super().dispatch(request, *args, **kwargs)


class DeviceCreateView(MixinPageTitle, MixinDevicePost, FormView):
    """View to create new Device.

    Args:
        form:
            type: ModelForm
            A DeviceCreateForm to create a Device
        form_action:
            type: str
            URL name for use in HTML form's action attribute in HTML detail page

    Returns:
        A new Device's instance and redirects to datacenter-rack-detail
    """

    model = Device
    template_name = "datacenter/device-detail.html"
    form_class = DeviceCreateForm
    action_name = "create"
    msg = ""

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_action"] = "datacenter:device-create"
        return context


class DeviceUpdateView(MixinPageTitle, MixinDevicePost, FormView):
    """View to update a Device or change its related Rack.

    Args:
        msg:
            type: str
            For display or log errors
        action_name:
            type: str
            Name to display in HMTL messages and title
        form_device:
            type: ModelForm
            A DeviceUpdateForm to update a Device
        form_action:
            type: str
            URL name for use in HTML form's action attribute in HTML detail page

    Returns:
        An updated Device's instance and redirects to datacenter-rack-detail
    """

    model = Device
    form_class = DeviceUpdateForm
    template_name = "datacenter/device-detail.html"
    action_name = "update"
    msg = ""

    def get_context_data(self, **kwargs):
        """Set context for HTML page."""
        context = super().get_context_data(**kwargs)
        context["pk_device"] = self.pk
        context["pk_rack"] = self.object.rack.pk
        # redeclaration of form_device in context in order
        # to have an update form instead of a create form for the device
        context["form_device"] = DeviceUpdateForm(instance=self.object)
        context["form_action"] = "datacenter:device-update"
        return context

    def dispatch(self, request, *args, **kwargs):
        self.pk = self.kwargs.get("pk_device", None)
        self.object = Device.objects.get(pk=self.pk)
        return super().dispatch(request, *args, **kwargs)


class DeviceDeleteView(DeleteView):
    """View to delete a Device from its Rack.

    Args:
        pk_rack:
            type: int
            The pk of the Rack's instance from which the device will be deleted.

    Returns:
        Redirects to datacenter:rack-update.
    """

    model = Device
    template_name = "datacenter/device_confirm_delete.html"

    def get_context_data(self, **kwargs):
        """Set context pk_rack variable for HTML page."""
        context = super().get_context_data(**kwargs)
        self.pk_rack = kwargs.get("rack")
        context["pk_rack"] = self.pk_rack
        return context

    def dispatch(self, request, *args, **kwargs):
        self.pk = self.kwargs.get("pk", None)
        self.object = Device.objects.get(pk=self.pk)
        self.pk_rack = self.kwargs.get("pk_rack", None)
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Override post method to redirect to rack."""
        # generate success message with variables
        self.success_message = self.get_success_message()
        # add message to context
        messages.add_message(
            self.request, messages.SUCCESS, self.success_message, "success"
        )
        # delete device
        self.object.delete()
        # redirect to rack's update view with pk_rack in kwargs
        return redirect(
            reverse("datacenter:rack-update", kwargs={"pk_rack": self.pk_rack})
        )

    def get_success_message(self, cleaned_data=None):
        """Override method to generate success_message after form validation."""
        self.success_message = (
            "%(model_name)s %(object)s has been deleted from Rack %(pk_rack)s."
        )
        model_name = self.model._meta.verbose_name  # get model's verbose name

        return self.success_message % dict(
            object=self.object,
            model_name=model_name,
            pk_rack=self.object.rack.pk,
        )


class RackViewSet(viewsets.ReadOnlyModelViewSet):
    """API endpoint that allows racks to be viewed in read-only mode."""

    queryset = Rack.objects.all().order_by("physical_location", "-id")
    serializer_class = RackSerializer

    if not ALLOW_API_READ_WITHOUT_AUTHENTICATION:
        permission_classes = [permissions.IsAuthenticated]


class DeviceViewSet(viewsets.ReadOnlyModelViewSet):
    """API endpoint that allows devices to be viewed in read-only mode."""

    queryset = Device.objects.all().order_by("-rack__id", "model_name")
    serializer_class = DeviceSerializer

    if not ALLOW_API_READ_WITHOUT_AUTHENTICATION:
        permission_classes = [permissions.IsAuthenticated]
