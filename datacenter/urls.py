from django.urls import path, re_path

from . import views

app_name = "datacenter"

urlpatterns = [
    path("home", views.RackListView.as_view(), name="datacenter-home"),
    re_path(
        r"devices-list", views.DevicesList.as_view(), name="datacenter-devices-list"
    ),
    re_path(
        r"rack-create/(?P<pk>[0-9]+)*",
        views.RackCreateView.as_view(),
        name="rack-create",
    ),
    re_path(
        r"rack-update/(?P<pk>[0-9]+)*",
        views.RackUpdateView.as_view(),
        name="rack-update",
    ),
    re_path(
        r"device-create/(?P<pk_device>[0-9]+)*/(?P<pk_rack>[0-9]+)*",
        views.DeviceCreateView.as_view(),
        name="device-create",
    ),
    re_path(
        r"device-update/(?P<pk_device>[0-9]+)*/(?P<pk_rack>[0-9]+)*",
        views.DeviceUpdateView.as_view(),
        name="device-update",
    ),
    re_path(
        r"device-delete-from-rack/(?P<pk>[0-9]+)*/(?P<pk_rack>[0-9]+)*",
        views.DeviceDeleteView.as_view(),
        name="device-delete-from-rack",
    ),
]
