import logging

from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView

from .forms import DeviceCreateForm, DeviceUpdateForm, RackForm
from .models.main import Rack

logger = logging.getLogger(__name__)


class MixinPageTitle:

    def get_context_data(self, **kwargs):
        """Set context page title and variable form_device for HTML page."""
        context = super().get_context_data(**kwargs)

        context["form_device"] = DeviceCreateForm

        if isinstance(self, ListView):
            context["page_title"] = (
                f"{self.action_name.capitalize()} {self.model._meta.verbose_name_plural}"
            )
        else:
            context["page_title"] = (
                f"{self.action_name.capitalize()} {self.model._meta.verbose_name}"
            )

        return context


class MixinFormErrorsMsg:
    """Greps form.errors to be used by logging, and to display errors from pre_save signals'."""

    def add_form_error_msg(self, form):
        if form.errors or form.non_field_errors:
            for field in form.errors:
                if field != "__all__":
                    self.msg += f"{field}: {form.errors[field]} "
                else:
                    self.msg += f"{form.errors[field]} "

        return self.msg


class MixinSuccessMsg(SuccessMessageMixin):
    """Override SuccessMessageMixin in order to customize variables in message."""

    success_message = "%(action_name)s %(model)s : %(object)s"

    def get_success_message(self, cleaned_data):
        """Method to generate success_message after form validation"""
        model_name = self.model._meta.verbose_name  # get model's verbose name
        action_name = f"{self.action_name.upper()}D"  # get variable create or update action from CBV

        return self.success_message % dict(
            cleaned_data,
            object=self.object,
            model=model_name,
            action_name=action_name,
        )


class MixinDevicePost(MixinSuccessMsg, MixinFormErrorsMsg):
    """ "Mixin used to override post in create and update device's views with error logging and messages in HTML templates.

    Args:
        pk_device: self.object.pk if exists
        success_url: html page for rack's detail
        success_message: string declaration of the variable that will be used in CBV
        msg: empty string declaration of the variable that will be used in CBV
             to format errors for logging
        form: DeviceCreateForm() or DeviceUpdateForm() depending of existing pk_device in kwargs

    Returns:
        reverse pk_rack in kwargs redirect with device.rack.pk in case of success
        redirects to success_message and success_url or render sender's page on errors
    """

    def __init__(self):
        self.msg = ""

    def post(self, request, *args, **kwargs):
        # case device already exists: check if pk_device is in kwargs
        if kwargs.get("pk_device"):
            # object should have been dispatch: check if object exists
            if self.object is not None:
                # assign object to update form for device
                self.form = DeviceUpdateForm(request.POST, instance=self.object)
            else:
                # fulfill form with request post data's only
                self.form = DeviceCreateForm(request.POST)
        else:
            # case device creation: select creation form for device
            self.form = DeviceCreateForm(request.POST)

        if self.form.is_valid():
            try:
                # do not commit the changes yet
                self.object = self.form.save(commit=False)
                # case device creation: check if pk_rack is in kwargs
                if kwargs.get("pk_rack"):
                    # set variable pk_rack
                    self.pk_rack = kwargs.get("pk_rack")
                    # queryset for this rack's id in database
                    # choice of filter against get to avoid try/except pattern
                    self.rack = Rack.objects.filter(pk=self.pk_rack)
                    # if queryset is not empty
                    if self.rack != []:
                        # get the first index of the queryset
                        self.rack = self.rack[0]
                        # assign this rack's instance to the new device
                        self.object.rack = self.rack
                # in all cases: save the device's instance
                self.object.save()
            except Exception as e:
                self.msg += str(e)
        else:
            self.msg += self.add_form_error_msg()

        if self.msg:
            # add message ERROR to display errors from pre_save signal
            # that are not showed by non_field_errors in template
            messages.add_message(
                self.request,
                messages.ERROR,
                self.msg,
                "danger",
            )
            # returns the cbv whitch is calling the method create for create and update for update
            logger.error(f"device creation/modification ERRORS: {self.msg}")

        else:
            # define success_message
            self.success_message = (
                "%(model)s: %(object)s was %(action_name)sd successfully"
            )
            # add the success message with variables' to the template render
            self.success_message = self.get_success_message(self.form.cleaned_data)

        # in all cases, set the url to which the redirects will be done with the pk_rack in kwargs
        self.success_url = reverse_lazy(
            "datacenter:rack-update", kwargs={"pk": self.object.rack.pk}
        )

        return super().post(self, request, *args, **kwargs)


class MixinRackPost(MixinSuccessMsg, MixinFormErrorsMsg):

    def post(self, request, *args, **kwargs):
        self.form = RackForm(request.POST, instance=self.object)

        if self.form.is_valid():
            try:
                self.object = self.form.save()

            except Exception as e:
                self.msg += str(e)
                logger.error(f"post exception :::: {str(e)}")
        else:
            self.msg += self.add_form_error_msg(self.form)

            logger.error(f"rack self.msg: {self.msg}")

        if self.msg:
            messages.add_message(
                self.request,
                messages.ERROR,
                self.msg,
                "danger",
            )
            if kwargs.get("pk") and self.msg != "":
                # needs to redirect in order to display error message
                return redirect("datacenter:rack-update", pk=kwargs.get("pk"))
        # if no errors, returns with success message
        return super().post(self, request, *args, **kwargs)
