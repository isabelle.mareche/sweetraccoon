from django.db.models.signals import pre_save
from django.dispatch import receiver

from .models.main import Device, Rack


@receiver(pre_save, dispatch_uid="device_pre_save_signal", sender=Device)
def device_pre_save(sender, **kwargs):
    """Checks if the vertical_size of the device will fits into the rack's available vertical units."""
    instance = kwargs["instance"]
    old_device_size = None
    new_device_size = None

    # case updating the device
    if instance.pk != None:
        # get the device as it is in the database before update
        old_fields = sender.objects.filter(pk=instance.pk)
        if old_fields:
            if old_fields[0]:
                if old_fields[0].vertical_size != instance.vertical_size:
                    # assign the old and the new value to respective variable
                    old_device_size = old_fields[0].vertical_size
                    new_device_size = instance.vertical_size

    # in all cases, check if the device fit in its associated rack
    instance.calculate_overflow(
        old_device_size=old_device_size,
        new_device_size=new_device_size,
        old_rack_available_units=None,
        new_rack_available_units=None,
    )


@receiver(pre_save, dispatch_uid="rack_pre_save_signal", sender=Rack)
def rack_pre_save(sender, **kwargs):
    """Prevents saving of instance's Rack if updating rack's vertical size conflicts with devices vertical sizes."""
    instance = kwargs["instance"]
    old_rack_available_units = None
    new_rack_available_units = instance.available_vertical_units

    # case updating the rack's available vertical units field
    if instance.pk != None:
        # get the rack as it is in the database before update
        old_fields = sender.objects.filter(pk=instance.pk)
        if old_fields:
            if old_fields[0]:
                # check if available vertical units' value has changed
                if (
                    old_fields[0].available_vertical_units
                    != instance.available_vertical_units
                ):
                    old_rack_available_units = old_fields[0].available_vertical_units
                    new_rack_available_units = instance.available_vertical_units

                for device in instance.devices_set.all():
                    # check if each of the device fit in this rack
                    device.calculate_overflow(
                        old_device_size=None,
                        new_device_size=None,
                        old_rack_available_units=old_rack_available_units,
                        new_rack_available_units=new_rack_available_units,
                    )
