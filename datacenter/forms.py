from crispy_bootstrap5.bootstrap5 import FloatingField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from django.forms import ModelForm

from datacenter.models.main import Device, Rack


class RackForm(ModelForm):
    """Rack form for creation and update using crispy forms."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()  # crispy

        self.helper.form_tag = False  # False -> render fields without form balise to include several forms in the same page
        self.helper.form_show_errors = True

        # add prefix to distinguish rack's in_state field from device's one in POST
        self.prefix = "rack"

        self.helper.layout = Layout(
            FloatingField("physical_location", css_class="my-2"),
            FloatingField("available_vertical_units", css_class="my-2"),
            FloatingField("in_state", css_class="my-2"),
        )

    class Meta:
        model = Rack
        fields = "__all__"


class DeviceCreateForm(ModelForm):
    """Device form for creation with field rack not displayed and not required."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # add prefix to distinguish device's in_state field from rack's one in POST
        self.prefix = "device"

        self.helper = FormHelper()  # crispy

        self.helper.form_tag = False  # False -> render fields without form balise to include several forms in the same page
        self.helper.form_show_errors = True

        # do not display field for rack
        self.helper.layout = Layout(
            FloatingField("device_type", css_class="my-2"),
            FloatingField("model_name", css_class="my-2"),
            FloatingField("number_of_ports", css_class="my-2"),
            FloatingField("vertical_size", css_class="my-2"),
            FloatingField("in_state", css_class="my-2"),
        )
        # rack will be added on save in post method
        # it should not be required for form_valid
        self.fields["rack"].required = False

    class Meta:
        model = Device
        fields = "__all__"


class DeviceUpdateForm(DeviceCreateForm):
    """Device form for update with field rack visible and required."""

    def __init__(self, *args, **kwargs):
        # heritate from DeviceCreateForm
        super().__init__(*args, **kwargs)

        # set required attribute for field rack
        self.fields["rack"].required = True

        # redefine layout with additional field for rack
        self.helper.layout = Layout(
            FloatingField("device_type", css_class="my-2"),
            FloatingField("model_name", css_class="my-2"),
            FloatingField("number_of_ports", css_class="my-2"),
            FloatingField("vertical_size", css_class="my-2"),
            FloatingField("in_state", css_class="my-2"),
            FloatingField("rack", css_class="my-2"),
        )
