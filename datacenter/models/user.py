from django.contrib.auth.models import AbstractUser, Group, Permission
from django.db import models
from simple_history.models import HistoricalRecords


class User(AbstractUser):

    history = HistoricalRecords(inherit=True, cascade_delete_history=True)

    groups = models.ManyToManyField(
        Group,
        verbose_name="localgroups",
        blank=True,
        help_text="Le groupe auquel cet utilisateur appartient",
        related_name="localuser_set",
        related_query_name="localuser",
    )

    user_permissions = models.ManyToManyField(
        Permission,
        verbose_name="localpermissions",
        blank=True,
        help_text="Permissions spécifiques à cet utilisateur.",
        related_name="localuser_set",
        related_query_name="localuser",
    )

    def __str__(self):
        return f"{self.last_name} {self.first_name}"
