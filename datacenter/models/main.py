import logging

from core.settings.main import (
    IN_STATE_COLORS_TAGS,
    IN_STATE_DEVICES_TO_EXCLUDE_INSIDE_RACKS,
    IN_STATE_TO_COUNT_IN_USED_UNITS,
    IN_STATE_TO_EXCLUDE_IN_USED_UNITS,
)
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Count, Q, Sum
from django.shortcuts import reverse
from simple_history.models import HistoricalRecords

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class LifeCycleField(models.Model):
    """Lifecycle state's that should be used for both racks and devices.

    Attributes:
        in_state: A string that discribes in which state of its lifecycle is the rack.
        to_count_in_used_units: A list of StateIn's values that shall be counted in querysets.
        to_exclude_in_used_units: A list of StateIn's values that shall be excluded from querysets.
    """

    class StateIn(models.TextChoices):
        PLANNING = "PLA", "Planning"
        PROCUREMENT = "PRO", "Procurement"
        PRESENT = "PRE", "Present"
        MAINTENANCE = "MAI", "Maintenance"
        REMOVED = "REM", "Removed"
        DECOMMISSIONING = "DEC", "Decommissioning"

    in_state = models.CharField(
        max_length=3,
        choices=StateIn,
        default=StateIn.PRESENT,
        verbose_name="Lifecycle State",
    )

    to_count_in_used_units = IN_STATE_TO_COUNT_IN_USED_UNITS
    to_exclude_in_used_units = IN_STATE_TO_EXCLUDE_IN_USED_UNITS
    not_displayed_states = IN_STATE_DEVICES_TO_EXCLUDE_INSIDE_RACKS
    in_state_colors_tags = IN_STATE_COLORS_TAGS

    class Meta:
        abstract = True


class DeviceCustomManager(models.Manager):
    """Manager for pre-selected Device queryset"""

    def get_active_devices(self):
        return (
            super().get_queryset().exclude(in_state__in=Rack.to_exclude_in_used_units)
        )


class RackCustomManager(models.Manager):
    """Manager for pre-selected Rack queryset"""

    def get_annotate_racks(self):
        """Get only active racks and annotate total actives devices and sum vertical_size for actives devices only."""

        return (
            super()
            .get_queryset()
            .exclude(in_state__in=LifeCycleField.to_exclude_in_used_units)
            .annotate(
                used_units=Sum(
                    "devices__vertical_size",
                    filter=Q(
                        devices__in_state__in=LifeCycleField.to_count_in_used_units
                    ),
                ),
                total_devices=Count(
                    "devices",
                    filter=Q(
                        devices__in_state__in=LifeCycleField.to_count_in_used_units
                    ),
                ),
            )
        )


class Rack(LifeCycleField):
    """Physical rack in datacenter.

    A rack unit (abbreviated U or RU) is a unit of measure defined as 1+3⁄4 inches (44.45 mm)
    a 1U front panel would be 123⁄32 inches (1.71875 in or 43.66 mm) tall.

    Attributes:
        in_state: A string that discribes in which state of its lifecycle is the rack.
        physical_location: A string indicates the place to find the rack in datacenter.
        available_vertical_units: An integer in U, representing the maximum number of vertical units in the rack.
        history: A HistoricalRecords instance for the instance'rack changes.
        objects: A custom manager for the class Rack.
    """

    physical_location = models.CharField(max_length=50, unique=True)
    available_vertical_units = models.IntegerField()

    # uses django-simple-history to save changes in instances
    history = HistoricalRecords(inherit=True, cascade_delete_history=True)

    # define a custom manager in order to define queryset
    # at model's level instead of view's level
    objects = RackCustomManager()

    class Meta:
        verbose_name = "Rack"
        verbose_name_plural = "Racks"
        ordering = ["physical_location"]

    def __str__(self):
        """Define how the Rack's instance is displayed when called."""
        return f"{self.physical_location} [{self.available_vertical_units}U]"

    def get_absolute_url(self):
        """Reverse pk for Rack's instance in Rack's update view's URL."""
        return reverse("datacenter:rack-update", kwargs={"pk": self.pk})

    @property
    def get_used_units(self):
        """Returns number of used units in rack different than in_state defined in config."""
        return (
            self.devices_set.exclude(
                in_state__in=LifeCycleField.to_exclude_in_used_units
            )
            .values("vertical_size")
            .aggregate(Sum("vertical_size"))["vertical_size__sum"]
        )


class Device(LifeCycleField):
    """Physical device in rack.

    A rack unit (abbreviated U or RU) is a unit of measure defined as 1+3⁄4 inches (44.45 mm)
    a 1U front panel would be 123⁄32 inches (1.71875 in or 43.66 mm) tall.

    Attributes:
        in_state: A string that discribes in which state of its lifecycle is the rack.
        device_type: A string that distinguish between two types: switch or router.
        model_name: A string that identifies the device.
        number_of_ports: An integer, indicates the number of ports available for the device.
        vertical_size: An integer in U, representing the number of vertical units that will be occupied by the device.
        history: A HistoricalRecords instance for the instance'rack changes.
        objects: A custom manager for the class Rack.
    """

    class TypeDevice(models.TextChoices):
        SWITCH = "S", "Switch"
        ROUTER = "R", "Router"

    device_type = models.CharField(
        max_length=2,
        choices=TypeDevice,
        default=TypeDevice.SWITCH,
    )

    model_name = models.CharField(max_length=50)
    number_of_ports = models.IntegerField()
    vertical_size = models.IntegerField()

    rack = models.ForeignKey(
        "Rack",
        verbose_name="rack",
        null=True,
        related_name="devices_set",
        related_query_name="devices",
        on_delete=models.PROTECT,
    )

    # uses django-simple-history to save changes in instances
    history = HistoricalRecords(inherit=True, cascade_delete_history=True)

    # define a custom manager in order to define queryset
    # at model's level instead of view's level
    objects = DeviceCustomManager()

    class Meta:
        verbose_name = "Device"
        verbose_name_plural = "Devices"
        ordering = ["rack__physical_location", "-pk"]

        # is this constraint pertinent?
        models.UniqueConstraint(
            name="unique_device",
            fields=["device_type", "model_name", "number_of_ports", "vertical_size"],
        )

    def __str__(self):
        """Define how the Device's instance is displayed when called."""
        return f"{self.model_name} ({self.vertical_size}U)"

    def get_absolute_url(self):
        """Reverse pk for Device's instance in Device's update view's URL."""
        return reverse("datacenter:device-update", kwargs={"pk_device": self.pk})

    def calculate_overflow(
        self,
        old_device_size=None,
        new_device_size=None,
        old_rack_available_units=None,
        new_rack_available_units=None,
    ):
        """Method used in pre_save signal for Device and Rack in order
        to prevent saving if available vertical units value is not sufficent for the associated devices.
        """
        logger.debug(f"CASE in CALCULATE_OVERFLOW {self.pk} {self.id}")
        logger.debug(f"CASE in CALCULATE_OVERFLOW \told_device_size\t{old_device_size}")
        logger.debug(f"CASE in CALCULATE_OVERFLOW \tnew_device_size\t{new_device_size}")
        logger.debug(
            f"CASE in CALCULATE_OVERFLOW \told_rack_available_units\t{old_rack_available_units}"
        )
        logger.debug(
            f"CASE in CALCULATE_OVERFLOW \tnew_rack_available_units\t{new_rack_available_units}"
        )

        # calculate all units used minus the size of the device which is in save process
        # according to the list of excluded in_state from count defined in core/settings/main.py
        used_units = self.rack.get_used_units

        logger.debug(
            f"IN case 1. self: {self}, pk: {self.pk}, self.vertical_size: {self.vertical_size} used_units: {used_units}"
        )

        # assign the size of the device which is in save process to the variable if it's None
        if used_units is None:
            used_units = self.vertical_size

        if used_units is not None:
            # calculate total size
            if (old_device_size and new_device_size) and (
                old_device_size != new_device_size
            ):
                # if value has changed, substract old value and add new value
                used = used_units - old_device_size
            elif self.rack.devices_set.count() == 1:
                # do not substract anything if there is only one device in rack
                # the value at this point is the newly changed even if not already committed
                used = self.vertical_size
            else:
                # define the variable anyway if nothing has to be modified
                used = used_units

            if old_rack_available_units:

                logger.debug(
                    f"old_rack_available_units {old_rack_available_units} new_rack_available_units: {new_rack_available_units}"
                )
                if old_rack_available_units > new_rack_available_units:
                    still_available = self.rack.available_vertical_units - used_units

            available = self.rack.available_vertical_units

            # calculate how many units are still available
            still_available = available - used

            # recalculate used_units if value have changed
            if new_device_size:
                used_units = used + new_device_size
            else:
                used_units = used

            logger.debug(
                f"\tCASE BEFORE error old: {old_device_size}\tnew: {new_device_size},\tused: {used}, used_units: {used_units}, available: {still_available} available: {still_available}"
            )

            # if adding new instance's vertical_size overflows the remaining space, then display error
            if used_units > self.rack.available_vertical_units:
                logger.debug(
                    f"\tCASE error old: {old_device_size}\tnew: {new_device_size},\tused: {used}, used_units: {used_units}, available: {available}"
                )
                if still_available < 0:
                    still_available = f"OVERFLOW: {abs(still_available)}"
                else:
                    still_available = f"MAX DEVICE SIZE: {still_available}"

                raise ValidationError(
                    "Used units: %(used_units)s but available vertical size in Rack «%(rack)s»: %(available)sU, %(still_available)s. NOT SAVED",
                    code="invalid",
                    params={
                        "rack": self.rack,
                        "available": self.rack.available_vertical_units,
                        "used": used,
                        "still_available": still_available,
                        "used_units": used_units,
                    },
                )
