from core.settings.main import ENVIRON_NAME, PROJECT_NAME
from django import template

register = template.Library()


@register.simple_tag
def get_environ_name():
    return ENVIRON_NAME.upper()


@register.simple_tag
def get_project_name():
    return PROJECT_NAME.upper()
