from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework import routers

from datacenter.views import DeviceViewSet, RackListView, RackViewSet

router = routers.DefaultRouter()
router.register(r"racks", RackViewSet)
router.register(r"devices", DeviceViewSet)

urlpatterns = [
    path("datacenter/admin/", admin.site.urls),
    path("datacenter/", include("datacenter.urls")),
    path("api/", include(router.urls)),
    path("api/racks", RackViewSet.as_view({"get": "list"}), name="drf-racks"),
    path("api/devices", DeviceViewSet.as_view({"get": "list"}), name="drf-devices"),
    re_path(r"|home", RackListView.as_view(), name="home"),
]
