# django-simple-history

# use uuid
SIMPLE_HISTORY_HISTORY_ID_USE_UUID = False  # default False

# disable revert
SIMPLE_HISTORY_REVERT_DISABLED = True  # default False

# disable indexing on history_date
SIMPLE_HISTORY_DATE_INDEX = True  # default True

# enable indexing on history_date (default setting)
SIMPLE_HISTORY_DATE_INDEX = True  # default True

# enable composite indexing on history_date and model pk (to improve as_of queries)
# the string is case-insensitive
SIMPLE_HISTORY_DATE_INDEX = "Composite"

# customize to accept a TextField model field for saving the history_change_reason
SIMPLE_HISTORY_HISTORY_CHANGE_REASON_USE_TEXT_FIELD = False  # default False

# change FileField to Charfield in historical tables
SIMPLE_HISTORY_FILEFIELD_TO_CHARFIELD = True
