import logging
import logging.config

logging.basicConfig(level=logging.INFO)

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "%(asctime)s - [Process : %(process)6s] - %(name)35s - %(levelname)8s - %(filename)20s - %(funcName)30s - ligne : %(lineno)5d - %(message)s"
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
        "file": {
            "level": "INFO",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "when": "D",
            "interval": 7,
            "backupCount": 2,
            "encoding": "UTF-8",
            "delay": False,
            "utc": False,
            "atTime": "W0",
            "formatter": "simple",
            "filename": "./logs/sweetraccoon.log",
        },
    },
    "loggers": {
        "core.settings.main": {
            "handlers": ["console", "file"],
            "propagate": False,
        },
        "datacenter.views": {
            "handlers": ["console", "file"],
            "propagate": False,
        },
        "datacenter.mixins": {
            "handlers": ["console", "file"],
            "propagate": False,
        },
        "datacenter.models.rack": {
            "handlers": ["console", "file"],
            "propagate": False,
        },
        "datacenter.models.device": {
            "handlers": ["console", "file"],
            "propagate": False,
        },
        "django.request": {
            "handlers": ["console"],
            "propagate": False,
        },
    },
}

logging.config.dictConfig(LOGGING)
