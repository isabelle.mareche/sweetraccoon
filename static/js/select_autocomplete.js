// autocomplete select modelform via input text
    // to be improved with arrows tabs for instance

// function to remove diacritics from string
function unaccent(string){
    return string.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
}

// function to display select with appropriate value and mask previous input
const setSelectValue = function (event, optionArray, input, res, selectReal) {
    optionArray.forEach((element, index) => {
        if (event.target.textContent === element.textContent) {
            // select option in hidden form field select
            selectReal.options[index].selected = "selected";
            // display the same value in input field
            input.value = element.textContent;
            res.style.display = "none";
            res.innerHTML = '';
            list = '';
            return selectReal;
        }
    })
}

// function to search matches in modelform options
function autocompleteMatchSelectOptions(input, array, divNewEntry, inputDiv) {
    if (input == '' || input == ' ') {
    return [];
    }
    // remove diacritics
    const normalized_input = unaccent(input)
    // console.log(`str : ${unaccent(input)}`);

    // create insensitive case regex
    const reg = new RegExp(normalized_input, "i");

    firstResult = array.filter(element => {
            if (unaccent(element.textContent).match(reg)) {
                //console.log(`REG element: ${element.textContent}`);
                return element;
            } else if (input.length >= 2) {
                // display block for new entry
                let divNew = document.querySelector(`#${divNewEntry}`);
                divNew.style.display = "block";
                // display none for input text and label
            }
        });
        return firstResult;
    };

    // function to get appropriate options and display matching results
    function showResultsSelectOptions(val, id_src, id_res, divNewEntry) {

        let inputDiv = document.querySelector(`#${id_src}`);
        let res = document.querySelector(`#${id_res}`);

        // get real select id by removing suffix from variable
        let realId = id_src.split("_input")[0];
        //console.log(`realId : ${realId}`);

        let selectRealId = document.querySelector(`#${realId}`);
        selectRealId.style.display = "none";
        //console.log(`selectRealId  : ${selectRealId.id}`);

        // get select options from hidden form field select
        let optionsFromId = document.querySelector(`#${realId}`).options;
        // console.log(`optionsFromId  : ${optionsFromId}`);

        // transforme to Array
        let arrayFromOptions = Array.from(optionsFromId);
        //console.log(`arrayFromOptions  : ${arrayFromOptions}`);

        // clean result's div and list
        res.innerHTML = '';
        res.classList = "result"; // col-xxl-3 col-xl-3 col-lg-3 col-md-5 col-sm-6 col-xs-6
        let list = '';
        let terms = autocompleteMatchSelectOptions(val, arrayFromOptions, divNewEntry, inputDiv);
        // add <li> for each terms' element
        terms.forEach(element => {
            list += `<li>${element.textContent}</li>`;
        })
        // add list to <ul>
        res.innerHTML = `<ul style="list-style-type: none;padding: 0;margin: 0;">${list}</ul>`;

        if (terms != undefined && terms.length > 0) {
            res.style.display = "block";
            // mask new entry block if exists
            let divNew = document.querySelector(`#${divNewEntry}`);
            //console.log(`divNewEntry::: ${divNewEntry}`);
            //divNew.style.display = "none";
        } else {
            res.style.display = "none";
        }

        // listen to click on results and set selected value for select
        res.addEventListener("click", (event) => {
            setSelectValue(event, arrayFromOptions, inputDiv, res, selectRealId);
        })

    }